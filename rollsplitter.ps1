# Roll splitter!


# Function to read an ini file, taken from Microsoft's blog
function Get-IniContent ($filePath)
{
    if (! (Test-Path -Path $FilePath -PathType Leaf)) {
        Write-Information -InformationAction Stop "$filePath does not exist."
    }
    $ini = @{}
    switch -regex -file $FilePath
    {
    "^\[(.+)\]" # Section
    {
        $section = $matches[1]
        $ini[$section] = @{}
        $CommentCount = 0
    }
    "^(;.*)$" # Comment
    {
        $value = $matches[1]
        $CommentCount = $CommentCount + 1
        $name = "Comment" + $CommentCount
        $ini[$section][$name] = $value
    }
    "\s*(.+?)\s*=\s*(.*)$" # Key
    {
        $name,$value = $matches[1..2]
        $ini[$section][$name] = $value
    }
    }
    $ini
}

function getVal($ini, $section, $name) {
    if (! $ini.ContainsKey($section)) {
        Write-Information -InformationAction Stop "[$section] section not found in ini file!"
    }
    if (! $ini[$section].ContainsKey($name)) {
        Write-Information -InformationAction Stop "[$section]:$name not found in ini file!"
    }
    $ini[$section][$name].trim()
}

# be flexible about the value of a boolean
function getBool($boolString) {
    switch ($boolString) {
    "0" { $False }
    "no" { $False }
    "n" { $False }
    "false" { $False }

    "1" { $True }
    "yes" { $True }
    "y" { $True }
    "true" { $True }

    default { Write-Information -InformationAction Stop ("Illegal boolean value found in ini file: " + $boolString) }
    }
}

$ini = Get-IniContent("rollsplitter.ini")
$logini = Get-IniContent("rollsplitter_logfile.ini")

$character_name = getVal $logini "rollsplitter_logfile" "character"
$server_name = getVal $logini "rollsplitter_logfile" "server"
$log_file_folder = (getVal $logini "rollsplitter_logfile" "folder") + "\Logs"
$html = getBool (getVal $ini "rollsplitter" "html")
$minutes_of_history = [int](getVal $ini "rollsplitter" "minutes_of_history")
$seconds_of_history = $minutes_of_history * 60
$log_file_path = $log_file_folder + "\eqlog_" + $character_name + "_" + $server_name + ".txt"
$friends = getVal $ini "rollsplitter" "friends"
$friends = $friends -replace '\s',''
$friends = $friends.Split(',')
$friends_hash = @{}
foreach ($friend in $friends) {
    $friends_hash[$friend] = $True
}
$cell_format = getVal $ini "rollsplitter" "cell_format"

$enable_tail = getBool (getVal $ini "rollsplitter" "enable_tail")
if ($enable_tail) {
    $tail_length = getVal $ini "rollsplitter" "tail_length"
}

echo "Reading log file ${log_file_path}..."
    
if ($enable_tail) {
    $log_array = Get-Content -Tail $tail_length -Path $log_file_path
} else {
    $log_array = Get-Content -Path $log_file_path
}

$timestamp_pat = "\[\w+ \w+ \d+ (?<hours>\d+):(?<minutes>\d+):(?<seconds>\d+) \d+\] "

function line_timestamp_to_sec($line) {
    if ( $line -match "^${timestamp_pat}" ) {
        [int]$Matches.hours * 3600 + [int]$Matches.minutes * 60 + [int]$Matches.seconds
    } else {
        Write-Information "Error: supplied log line doesn't have a valid timestamp: $line" -InformationAction Stop
    }
}

function find_start {
    $final_time = line_timestamp_to_sec $log_array[-1]
    $line = 1
    foreach ($number in 2..$log_array.Count) {
        $this_time = line_timestamp_to_sec $log_array[-$line]
        $time_delta = $final_time - $this_time
        if ($time_delta -lt 0) {
            $time_delta += 24 * 3600;
        }
        if ( $time_delta -gt $seconds_of_history ) {
            return $line
        }
        $line++
    }
    Write-Information "Warning: Log file appears shorter than minutes_of_history" -InformationAction Continue
    $line
}

$beginning = find_start
# process lines from $beginning till the end of the file
$roll_pat="^${timestamp_pat}\*\*A Magic Die is rolled by (?<player>\w+)\. It could have been any number from 0 to (?<rollmax>\d+)\, but this time it turned up a (?<roll>\d+)\.$"

$announce_pat = "^${timestamp_pat}(\w+ tells the|You tell your) (party|raid), '/ran +(?<rollmax>[0-9]+) (?<item>[a-zA-Z0-9``#%&()-=+|':.,/? ]+)'"

# items is a hash table, keyed by rollmax.  Each element is the name of the item for that rollmax
$items = @{}

# rolls is a hash table, also keyed by rollmax.  Each element is an array of (player, roll)'s
$rolls = @{}
$roll_num = 1
foreach ($line in $beginning..1) {
    if ( $log_array[-$line] -match $roll_pat ) {
#                                     0               1             2
        $rolls[$Matches.rollmax] += ,($Matches.player,$Matches.roll,$roll_num)
    $roll_num += 1
    } else {
        if ( $log_array[-$line] -match $announce_pat ) {
            $items[$Matches.rollmax] = $Matches.item
        }
    }
}
# convert hash table to 2D array where each row contains a rollmax, roll, player
$roll_arr = $()
foreach ($rm in $rolls.Keys) {
    foreach ($player_roll in $rolls[$rm]) {
#                      0   1               2               3
        $roll_arr += ,($rm,$player_roll[1],$player_roll[0],$player_roll[2])
    }
}

$scale = 1000000
# Sort each set of rolls by the rollmax (major) and (rollmax - roll)
# (minor).  To do this we multiply rollmax field by $scale to make it take
# higher precedence over the roll, which is subtracted from rollmax to
# comprise the minor part of the sort key.

$sorted_arr = $()
$roll_arr | Sort-Object @{Expression={([int64]$_[0] * [int64]$scale) + ([int64]$_[0] - [int64]$_[1])}; Ascending=$True} | ForEach-Object {
    $sorted_arr += ,$_
}

# Check for duplicate rolls on the same rollmax
# To do this, we create a hash table keyed by rollmax:player name.
# The value of the hash will be the count for that player.
# Counts above one are determined to be duplicates.
$duplicates = @{}
foreach ($roll in $sorted_arr) {
    $key = $roll[0] + ":" + $roll[2]
    if ($duplicates.Contains($key)) {
        Write-Information -InformationAction Continue "Found duplicate $key"
        $duplicates[$key] += 1
    } else {
        $duplicates[$key] = 1
    }
}

function is_duplicate($rollmax, $player) {
    $key = $rollmax + ":" + $player
    $duplicates.ContainsKey($key) -and ($duplicates[$key] -gt 1)
}

function is_character($player) {
    $character_name.equals($player)
}

function is_friend($player) {
    $friends_hash.ContainsKey($player)
}

# Final pass.  Go over the array by finding the first element of each
# rollmax set, then the second, and so forth.  Some rollmax sets will have
# fewer entries than others, and we need to account for that.

$sorted_rms = $rolls.Keys | Sort-Object @{Expression={[int]$_}; Ascending=$True}
if ($html) {
    $fname=".\rolls.html"
    $file_start = "<!doctype html>`r`n<html lang='en' data-bs-theme='dark'>`r`n" +
                  "<head>`r`n" +
                  "<meta charset='utf-8' \>`r`n" +
                  "<meta name='viewport' content='width=device-width, initial-scale=1' \>`r`n" +
                  "<link href='https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css' rel='stylesheet' />`r`n" +
                  "<style>`r`ntd {`r`n  padding:5px`r`n}`r`n#duplicate {`r`n  color: #ff0000`r`n}`r`n#character {`r`n  color: #00f000`r`n}`r`n#friend {`r`n  color: #ffff00`r`n}`r`n</style>`r`n" +
                  "</head>`r`n" +
                  "<body>`r`n" +
                  "<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js' integrity='sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm' crossorigin='anonymous'>`r`n</script>"
    $file_end = "</body>`r`n</html>"
    $tbl_start = "<table border='2' class='table table-bordered table-responsive-xs'>"
    $tbl_end = "</table>"
    $hdr_start = "<th>"
    $hdr_end = "</th>"
    $row_start = "<tr>"
    $row_end = "</tr>"
    $col_start = "<td>"
    $duplicate_col_start = "<td id='duplicate'>"
    $character_col_start = "<td id='character'>"
    $friend_col_start = "<td id='friend'>"
    $col_end = "</td>"
    $item_delim = "<br/>"
} else {
    $fname=".\rolls.csv"
    $file_start = ""
    $file_end = ""
    $tbl_start = ""
    $tbl_end = ""
    $hdr_start = ""
    $hdr_end = ","
    $row_start = ""
    $row_end = ""
    $col_start = ""
    $duplicate_col_start = ""
    $col_end = ","
    $item_delim = ":"
}
$file_start | Out-File -Encoding utf8 -FilePath $fname
# Write out the header columns, namely the different roll maxes.
$this_line = $tbl_start + $row_start
$first = $True
foreach ($rm in $sorted_rms) {
    $this_line += $hdr_start + $rm + $item_delim + $items[$rm] + $hdr_end
}
$this_line + $row_end | Out-File -Append -Encoding utf8 -FilePath $fname

for ($idx = 0; $True ; $idx++) {
    # find  element number idx of each rollmax set
    $this_line = $row_start
    $found_any = $False
    :rmloop foreach ($rm in $sorted_rms) {
        $this_idx = 0;
        foreach ($roll in $sorted_arr) {
            if ($rm -eq $roll[0]) {
                if ($this_idx -eq $idx) {
                    $found_any = $True
                    if (is_duplicate $rm $roll[2]) {
                        if (! $html) {
                            $tag = "!!!"
                        } else {
                            $tag = ""
                        }
                        $this_col_start = $duplicate_col_start
                    } else {
                        if (is_character $roll[2]) {
                            $this_col_start = $character_col_start
                        } elseif (is_friend $roll[2]) {
                            $this_col_start = $friend_col_start
                        } else {
                            $tag = ""
                            $this_col_start = $col_start
                        }
                    }
                    $this_line += $this_col_start + $tag + ($cell_format -f $roll[1..3]) + $col_end
                    continue rmloop
                } else {
                    $this_idx++
                }
            }
        }
        # didn't find one for this column.
        $this_line += $col_start + $col_end
    }
    if (! $found_any) {
        break
    }
    $this_line + $row_end | Out-File -Append -Encoding utf8 -FilePath $fname
}
$tbl_end | Out-File -Append -Encoding utf8 -FilePath $fname
$file_end | Out-File -Append -Encoding utf8 -FilePath $fname

if ($html) {
    Start-Process ([system.uri](Get-Item $fname).FullName).AbsoluteURI
} else {
    Start-Process $fname
}
