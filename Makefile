
all: README.pdf

README.pdf: README.md
	pandoc -f markdown -t pdf README.md > README.pdf
