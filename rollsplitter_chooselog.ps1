Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.IO

# Function to load settings from ini file
function Load-Settings {
    param ($iniFilePath)
    $settings = @{
        FolderPath = $null
        Server     = $null
        Character  = $null
    }
    if (Test-Path $iniFilePath) {
        $iniContent = Get-Content -Path $iniFilePath
        foreach ($line in $iniContent) {
            if ($line -match "folder=(.*)") {
                $settings.FolderPath = $matches[1]
            } elseif ($line -match "server=(.*)") {
                $settings.Server = $matches[1]
            } elseif ($line -match "character=(.*)") {
                $settings.Character = $matches[1]
            }
        }
    }
    return $settings
}

# Function to choose a folder
function Choose-Folder {
    param ($initialPath)
    $folderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog]
    $folderBrowser.Description = "Choose the Everquest folder"
    $folderBrowser.ShowNewFolderButton = $false
    if ($initialPath -and (Test-Path $initialPath)) {
        $folderBrowser.SelectedPath = $initialPath
    }
    if ($folderBrowser.ShowDialog() -eq [System.Windows.Forms.DialogResult]::OK) {
        return $folderBrowser.SelectedPath
    }
    return $null
}

# Function to find files with specific format
function Find-LogFiles {
    param ($folderPath)
    $logsPath = $folderPath + "\Logs"
    Get-ChildItem -Path $logsPath -Filter "eqlog_*.txt" | ForEach-Object {
        if ($_ -match "eqlog_([^_]+)_([^_]+)\.txt") {
            [PSCustomObject]@{
                Character = $matches[1]
                Server    = $matches[2]
                FileName  = $_.FullName
            }
        }
    }
}

# Function to select server and character
function Select-ServerAndCharacter {
    param ($logFiles, $initialServer, $initialCharacter)
    $form = New-Object System.Windows.Forms.Form
    $form.Text = "Select Server and Character"
    $form.Width = 400
    $form.Height = 300

    $labelServer = New-Object System.Windows.Forms.Label
    $labelServer.Text = "Server:"
    $labelServer.Top = 20
    $labelServer.Left = 20
    $form.Controls.Add($labelServer)

    $comboServer = New-Object System.Windows.Forms.ComboBox
    $comboServer.Top = 50
    $comboServer.Left = 20
    $comboServer.Width = 150
    $comboServer.DropDownStyle = [System.Windows.Forms.ComboBoxStyle]::DropDownList
    $form.Controls.Add($comboServer)

    $labelCharacter = New-Object System.Windows.Forms.Label
    $labelCharacter.Text = "Character:"
    $labelCharacter.Top = 100
    $labelCharacter.Left = 20
    $form.Controls.Add($labelCharacter)

    $comboCharacter = New-Object System.Windows.Forms.ComboBox
    $comboCharacter.Top = 130
    $comboCharacter.Left = 20
    $comboCharacter.Width = 150
    $comboCharacter.DropDownStyle = [System.Windows.Forms.ComboBoxStyle]::DropDownList
    $form.Controls.Add($comboCharacter)

    $buttonOK = New-Object System.Windows.Forms.Button
    $buttonOK.Text = "OK"
    $buttonOK.Top = 200
    $buttonOK.Left = 20
    $buttonOK.Width = 80
    $buttonOK.DialogResult = [System.Windows.Forms.DialogResult]::OK
    $form.Controls.Add($buttonOK)

    $form.AcceptButton = $buttonOK

    # Populate the server combo box
    $servers = $logFiles | Select-Object -ExpandProperty Server -Unique
    $comboServer.Items.AddRange($servers)
    if ($initialServer -and $comboServer.Items.Contains($initialServer)) {
        $comboServer.SelectedItem = $initialServer
    }

    # Populate the character combo box based on the initial server selection
    if ($initialServer) {
        $characters = $logFiles | Where-Object { $_.Server -eq $initialServer } | Select-Object -ExpandProperty Character
        $comboCharacter.Items.AddRange($characters)
        if ($initialCharacter -and $comboCharacter.Items.Contains($initialCharacter)) {
            $comboCharacter.SelectedItem = $initialCharacter
        }
    }

    $comboServer.Add_SelectedIndexChanged({
        $comboCharacter.Items.Clear()
        $selectedServer = $comboServer.SelectedItem
        if ($selectedServer) {
            $characters = $logFiles | Where-Object { $_.Server -eq $selectedServer } | Select-Object -ExpandProperty Character
            $comboCharacter.Items.AddRange($characters)
            if ($initialCharacter -and $comboCharacter.Items.Contains($initialCharacter)) {
                $comboCharacter.SelectedItem = $initialCharacter
            }
        }
    })

    if ($form.ShowDialog() -eq [System.Windows.Forms.DialogResult]::OK) {
        return [PSCustomObject]@{
            Server    = $comboServer.SelectedItem
            Character = $comboCharacter.SelectedItem
        }
    }
    return $null
}

# Write settings to ini file
function Write-Settings {
    param ($iniFilePath, $folderPath, $server, $character)
    $settings = @"
[rollsplitter_logfile]
folder=$folderPath
server=$server
character=$character
"@
    Set-Content -Path $iniFilePath -Value $settings
}

# Main script
$iniFilePath = "rollsplitter_logfile.ini"
$settings = Load-Settings -iniFilePath $iniFilePath

# Choose folder
$folderPath = Choose-Folder -initialPath $settings.FolderPath
if (-not $folderPath) {
    Write-Host "No folder selected. Exiting."
    exit
}

# Find log files
$logFiles = Find-LogFiles -folderPath $folderPath
if ($logFiles.Count -eq 0) {
    Write-Host "No log files found. Exiting."
    exit
}

# Select server and character
$selection = Select-ServerAndCharacter -logFiles $logFiles -initialServer $settings.Server -initialCharacter $settings.Character
if (-not $selection) {
    Write-Host "No selection made. Exiting."
    exit
}

# Get selected file path
$selectedFile = $logFiles | Where-Object { $_.Server -eq $selection.Server -and $_.Character -eq $selection.Character } | Select-Object -ExpandProperty FileName

# Write to ini file
Write-Settings -iniFilePath $iniFilePath -folderPath $folderPath -server $selection.Server -character $selection.Character

Write-Host "File path saved to ini file: $selectedFile"
